.. include:: images.rst

Create a patron category
_____________________________

All patrons in Koha must have a patron category. The patron category is like a jelly mold; multiple individual patron accounts can share common characteristics, such as circulation rules, notice frequency, holds allowed, and much more. Koha requires at least one patron category in order to add patrons. 

A patron category requires:

  * Category code
  * Description
  * Overdue notice required
  * Category type
  * Default privacy
  * Enrolment period: In months OR Until date

.. figure:: /images/create_patron_categories_1.*
   :alt: Create a patron category

   Create a patron category

1. **Category code:** Code consisting of 1 or 2 consecutive capital letters. [FIXME]

2. **Description:** Sentence describing what the patron category is.

3. *Overdue notice required* dropdown button: Set by default to 'No'. This specifies if you want the patron category to receive overdue notices.

4. **Category type:** Set by default to 'Staff'. This makes the category created a staff member.

5. **Default privacy:** Set by default to 'Default'. The Default privacy controls the amount of time that the reading history of the patron is stored for.

6. **Enrolment period:** This is the amount of time that the patrons created from this patron category are enrolled for. Either input a number of months or select a date from the interactive datepicker calendar icon.

7. Click the *Submit* button to create the patron category.

For example:

.. figure:: /images/create_patron_category_1_example.*
   :alt: Create a patron category example

   Create a patron category example
