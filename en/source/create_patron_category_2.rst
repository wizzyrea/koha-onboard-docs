.. include:: images.rst

Create a patron category outcome
____________________________________

This screen indicates whether the category was created successfully.

.. figure:: /images/create_patron_category_2.*
   :alt: Create patorn category outcome

   Create patron category outcome
   

1. **Create patron category message:** This indicates that the patron category was created successfully
2. **Path to create patron category:** More patron categories can be created or altered by going to Administration -> Patron categories
3. Click the *Add a patron* button to go to the next step
