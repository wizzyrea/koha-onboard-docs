.. include:: images.rst

Create a Item type outcome
_____________________________

This screen will tell you if the item type was created successfully.

.. figure:: images/create_item_type_2.*
   :alt: Create item type outcome

   Create item type outcome


1. **Create item type message:** This tells you if the item type was successfully created
2. **Path to create item type:** This is the path from the home page to create another item type or alter an existing one
3. Click the *Add a circulation rule* button to go to the next step
