.. include:: images.rst



Selected data added
____________________

After you have clicked the *Import>>* button in either the marc21 or UNIMARC setup screen this screen will appear. It shows you if the selected/default  data values were successfully inserted into the database.

.. figure::  images/data_added_1.*
   :alt: Selected data added

   Selected data added


1. **mysql data added:** These data values will always be installed and consequently displayed on this screen.

2. **mandatory data added:** Same as above.

3. **default data added:** If you used the *Basic setup* then this will contain multiple data values, if however you used the *Advanced setup* and did not select any default data values then this area will be empty.

4. **optional data added:** If you selected an optional data value then it will be displayed here. If you did not choose any optional data value then the **optional data added** title will not be displayed.

5. **Installation message:** Tells you if the Koha database was successfully created ready for you to use the onboarding tool.

6. Click the *Continue to Koha onboarding tool* button to go to a redirection screen, which in turn will take you to the onboarding tool.
