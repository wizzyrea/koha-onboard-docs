.. include:: images.rst

Introduction to the Koha Installation Process
================================================

The Koha Library Management System installation process uses 2 tools:

* Web installer
* Onboarding tool

This documentation will outline both of these tools.

What is the Koha web installer?
_________________________________
The web installer sets up the database tables that store the all the data you work with in Koha, for example the library branches, patrons and items.

The web installer starts with a login screen for you to enter the database administrator account credentials.

The installer will then create the tables and fill the tables with data. The next step in the process is to set up a few mandatory first use settings, via the Onboarding tool.


What is the Koha Onboarding tool?
_________________________________

The onboarding tool makes sure you have at least one library, patron category, patron, item type and circulation rule before you start using Koha.

If you installed sample data for library, patron category, and/or item type then the screens to create these will be skipped.

You will, however, always have to create a patron (to log into the staff interface at the end of the onboarding tool) and a circulation rule.

.. note::
  If, when using the web installer, you installed sample data, sample library data for example, then the onboarding tool will display a skip step message, like shown below:

.. figure:: /images/skipstep.*
   :alt: Onboarding tool skip screen message

   Example of the skip screen message
