.. include:: images.rst

Create a library
___________________
The installer will prompt to create the first library or branch if the sample libraries were not installed.

A library in Koha is the digital representation of a "physical" place. More libraries can be added later by going to Administration -> Libraries and groups.

.. figure:: /images/create_library_screen_1.*
   :alt: Create library

   Create library


1. **Library code:** code consisting of 3 consecutive capital letters. [FIXME]
2. **Name:** Official name of the library, as it is commonly known.
3. Click the *Submit* button to create a library.


For example:

.. figure:: /images/create_library_screen_1_example.*
   :alt: Create library example

   Create library example


  1. **Library code:** The regular expression that filters the acceptable inputs for library code only allows you to enter 3 consecutive capital letters. [FIXME]
  2. **Name:** The regular expression for this field will only accept letters and spaces.
