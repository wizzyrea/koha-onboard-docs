.. include:: images.rst

UNIMARC setup
__________________________________


UNIMARC Basic setup
~~~~~~~~~~~~~~~~~~~~~~

The basic setup of UNIMARC is the same as that of MARC21:

* You are only able to select/unselect the optional data values.
* Optional values are the same for both UNIMARC and MARC21, therefore their definition will not be repeated, please go to `web_installer_marc21_setup` to read the definitions.

.. figure:: /images/unimarc_basic_setup.*
   :alt: UNIMARC basic setup

   UNIMARC basic setup


1. Click the *Select all options* button to select all optional data values to be installed.

2. *Default:* As with the Basic setup for MARC21 the data values in this area are mandatory and selected by default.

3. *Optional:* As previously stated these are the same data values as defined in the *Basic setup* of MARC21.

4. Click the *Import>>* button to install the sample data.



UNIMARC Advanced setup
~~~~~~~~~~~~~~~~~~~~~~~~~~

There are only 2 data values in the UNIMARC advanced setup which are unique:

* Authority Structure for UNIMARC in English
* Default UNIMARC bibliographic framework in English

.. figure:: /images/unimarc_advanced_setup.*
   :alt: UNIMARC advanced setup

   UNIMARC advanced setup


1. Click the *Select all options* button to select all default and optional data values to be installed.

2. *Authority structure for UNIMARC in English:* Authority records store the search terms used to find items in the database. Unlike marc21, when using UNIMARC the authority structure needs to be compatible with UNIMARC. Selecting this option makes it compatible.

3. *Default UNIMARC bibliographic framework in English:* In the previous screen you clicked to use the UNIMARC flavour, now by selecting this option you are confirming you want to use that framework.

4. Click the *Import>>* button to install the sample data
